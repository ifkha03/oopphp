<?php

require_once 'Type.php';

class Food extends Type {
	protected $name;
	protected $price;
	protected $rating = '4.9/5';
	protected $type = type;

	protected function __construct($name, $price) {
		$this->type = $type;
		$this->name = $name;
		$this->price = $price;
		$this->rating = $rating;
	}

	protected function information() {
		echo 'Below is your ' . $this->type . ' information:<br>';
		echo 'Food Name &nbsp&nbsp: ' . $this->name . '<br>';
		echo 'Food Price &nbsp&nbsp&nbsp: ' . $this->price . '<br>';
		echo 'Food Rating &nbsp: ' . $this->rating . '<br>';
	}
}
?>